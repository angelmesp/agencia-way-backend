const mysql = require('mysql');

// Crear venta
exports.crearVenta = (req, res) => {
  const { productos, cliente } = req.body;
  const sql = 'CALL insertarVenta(?, ?)';
  const values = [productos,cliente];

  req.db.query(sql, values, (err, result) => {
    if (err) {
      res.status(500).json({ error: 'Error al crear la venta' });
    } else {
       res.json({ message: 'Se ha registrado una venta correctamente' });
    }
  });
};

exports.obtenerVentasID = (req, res) => {
  const { id } = req.params;
  const sqlVenta = `SELECT * FROM Venta v LEFT JOIN cliente c on c.ID_Cliente = v.ID_Cliente WHERE ID_Venta = ?;`;
  const sqlDetalle = `SELECT a.*, b.imagen FROM detalle_venta a left join producto b on a.ID_Producto = b.ID_Producto WHERE ID_Venta = ?;`;

  req.db.query(sqlVenta, [id], (errVenta, resultVenta) => {
    if (errVenta) {
      res.status(500).json({ error: 'Error al obtener la venta' });
    } else {
      if (resultVenta.length === 0) {
        res.status(404).json({ error: 'Venta no encontrada' });
        return;
      }

      req.db.query(sqlDetalle, [id], (errDetalle, resultDetalle) => {
        if (errDetalle) {
          res.status(500).json({ error: 'Error al obtener el detalle de la venta' });
        } else {


          const venta = resultVenta[0];
          const detalle = resultDetalle.map((detalleItem) => ({
            Nombre_Producto: detalleItem.Nombre_Producto,
            ID_Producto: detalleItem.ID_Producto,
            Cantidad: detalleItem.Cantidad,
            Precio_Unitario: detalleItem.Precio_Unitario,
            Subtotal: detalleItem.Subtotal,
             imagen: `http://localhost:3000/uploads/${detalleItem.imagen}`,
          }));

          const ventaConDetalle = {
            ...venta,
            Detalle: detalle,
          };

          res.json(ventaConDetalle);
        }
      });
    }
  });
};



// Obtener todas las ventas
exports.obtenerVentas = (req, res) => {
  const sql = 'SELECT * FROM Venta v LEFT JOIN cliente c on c.ID_Cliente = v.ID_Cliente ;';
  req.db.query(sql, (err, result) => {
    if (err) {
      res.status(500).json({ error: 'Error al obtener las ventas' });
    } else {
      res.json(result);
    }
  });
};
