const mysql = require('mysql');
const fs = require('fs');
const path = require('path');

exports.agregarProducto = (req, res) => {
  const { Nombre, Precio, Stock } = req.body;
  const imagen = req.file; 
  const sql = 'INSERT INTO Producto (Nombre, Precio, Stock, imagen) VALUES (?, ?, ?, ?)';
  const values = [Nombre, Precio, Stock, imagen.filename];

  req.db.query(sql, values, (err, result) => {
    if (err) {
      res.status(500).json({ error: 'Error al agregar el producto' });
    } else {
      res.json({ message: 'Producto actualizado exitosamente' });
    }
  });
};

// Eliminar producto
exports.eliminarProducto = (req, res) => {
  const { id } = req.params;
  const sql = 'CALL EliminarProducto(?)';

   req.db.query(sql, [id], (err, result) => {
    if (err) {
      res.status(500).json({ error: 'Error al eliminar el producto' });
    } else {
      res.json({ message: 'Producto eliminado exitosamente' });
    }
  });
};

exports.obtenerProductos = (req, res) => {
  const sql = 'SELECT * FROM Producto';
  req.db.query(sql, (err, result) => {
    if (err) {
      res.status(500).json({ error: 'Error al obtener los productos' });
    } else {
      const productosConURLImagen = result.map((producto) => ({
        ...producto,
        imagen: `http://localhost:3000/uploads/${producto.imagen}`,
      }));
      res.json(productosConURLImagen);
    }
  });
};

