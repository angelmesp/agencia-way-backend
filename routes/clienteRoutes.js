const express = require('express');
const router = express.Router();
const clienteController = require('../controllers/clienteController');

// Obtener todos los clientes
router.get('/clientes', clienteController.obtenerClientes);

// Agregar cliente
router.post('/clientes', clienteController.agregarCliente);


// Eliminar cliente
router.delete('/clientes/:id', clienteController.eliminarCliente);

module.exports = router;
