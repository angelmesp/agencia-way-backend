const express = require('express');
const router = express.Router();
const productoController = require('../controllers/productoController');
const multer = require('multer');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads'); // Asegúrate de reemplazar esto con la ruta adecuada
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    cb(null, file.fieldname + '-' + uniqueSuffix + '.' + file.originalname.split('.').pop());
  },
});

const upload = multer({ storage: storage });

// Configurar la ruta estática para servir las imágenes
router.use('/uploads', express.static('uploads'));

// Obtener todos los productos
router.get('/productos', productoController.obtenerProductos);

// Agregar producto
router.post('/productos', upload.single('imagen'), productoController.agregarProducto);

// Eliminar producto
router.delete('/productos/:id', productoController.eliminarProducto);

module.exports = router;
