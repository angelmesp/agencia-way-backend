const express = require('express');
const mysql = require('mysql');
const cors = require('cors');

const app = express();
const port = 3000;

app.use(cors());
app.use(express.json());

// Configurar conexión a la base de datos
const db = mysql.createConnection({
 host: 'localhost',
  user: 'root',
  password: '',
  database: 'ventas',
});

// Establecer conexión a la base de datos
db.connect((err) => {
  if (err) {
    throw err;
  }
  console.log('Conexión a la base de datos establecida');
});

// Adjuntar db al objeto req
app.use((req, res, next) => {
  req.db = db;
  next();
});

// Rutas de la API
const clienteRoutes = require('./routes/clienteRoutes');
const productoRoutes = require('./routes/productoRoutes');
const ventaRoutes = require('./routes/ventaRoutes');

app.use(clienteRoutes);
app.use(productoRoutes);
app.use(ventaRoutes);

// Iniciar el servidor
app.listen(port, () => {
  console.log(`Servidor escuchando en http://localhost:${port}`);
});
